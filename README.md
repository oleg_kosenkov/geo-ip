### GeoIP Web Service.
Provides a REST API to determine geolocation by an ip address.

###### Example request
GET http://servicehost/geoip/8.8.8.8

###### Example response
````
{  
  "canonicalIPv4Representation": "8.8.8.8",  
  "cityName": "Mountain View",  
  "countryCode": "US",  
  "countryName": "United States",  
  "IPv4": "134744072",  
  "latitude": "37.405992",  
  "longitude": "-122.078515",  
  "regionName": "California"  
}
````

This service includes IP2Location LITE data available from http://www.ip2location.com under the public license.
